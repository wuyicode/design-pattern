package com.atguigu.mediator;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Colleague {
    private Mediator mediator;
    public String name;

    public abstract void SendMessage(int stateChange);
}
