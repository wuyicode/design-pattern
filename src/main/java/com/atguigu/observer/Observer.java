package com.atguigu.observer;

public interface Observer {
    void update(float temperature, float pressure, float humidity);
}
