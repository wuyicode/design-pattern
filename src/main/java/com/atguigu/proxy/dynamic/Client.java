package com.atguigu.proxy.dynamic;

public class Client {

    public static void main(String[] args) {
        Object proxyInstance = new ProxyFactory(new TeacherDao()).getProxyInstance();
        ITeacherDao instance = (ITeacherDao) proxyInstance;
        System.out.println("proxyInstance=" + proxyInstance.getClass());

        // 两次调用方法， 也就意味着两次使用了invoke 反射。
        instance.teach();
        instance.sayHello("tom");
    }
}
