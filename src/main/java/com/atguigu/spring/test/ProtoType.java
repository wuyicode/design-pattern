package com.atguigu.spring.test;

import com.atguigu.spring.bean.Monster;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * spring的 IOC 容器管理，默认是 singleton单例。
 * 修改scope属性为 scope="prototype" ， 变为原型，也就是多例。
 */
public class ProtoType {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Monster id01 = (Monster) context.getBean("id01");
        Monster id02 = (Monster) context.getBean("id01");
        System.out.println(id01);
        System.out.println(id02);
        System.out.println(id01==id02);
    }
}
