package com.atguigu.prototype;

public class Client {
    public static void main(String[] args) throws CloneNotSupportedException {
        Sheep sheep = new Sheep("tom", 22, "white");
        sheep.setFriend(new Sheep("John", 32, "black"));
        Sheep clone = (Sheep)sheep.clone();
        System.out.println(sheep.getFriend().hashCode());
        System.out.println(clone.getFriend().hashCode());
    }
}
