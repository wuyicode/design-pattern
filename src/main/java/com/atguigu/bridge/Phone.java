package com.atguigu.bridge;

public abstract class Phone {
    /**
     * 这个 Brand 的作用 就有桥接的意思 。
     */
    private Brand brand;

    public Phone(Brand brand) {
        this.brand = brand;
    }

    protected void open() {
        brand.open();
    }

    protected void close() {
        brand.close();
    }

    protected void call() {
        brand.call();
    }

}
