package com.atguigu.decorator;

public class CoffeeBar {
    public static void main(String[] args) {
        Drink order = new Coffee();
        order = new Milk(order);
        order = new Chocolate(order);

        System.out.println("order 加入一份牛奶 加入一份巧克力  费用 =" + order.cost());
        System.out.println("order 加入一份牛奶 加入一份巧克力 描述 = " + order.getDes());
    }
}
