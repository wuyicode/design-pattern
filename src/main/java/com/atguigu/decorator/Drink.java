package com.atguigu.decorator;

import lombok.Data;

@Data
public abstract class Drink {
    public String des; // 描述
    private float price = 0.0f;
    public abstract float cost();   // 计算费用的抽象方法
}
