package com.atguigu.flyweight;

import java.util.HashMap;

/**
 * 享元设计模式，或者叫做 蝇量设计模式.
 * 典型的应用是 数据库的连接线程池。
 */
// 网站工厂类，根据需要返回压一个网站
public class WebSiteFactory {
    private HashMap<String, ConcreteWebSite> pool = new HashMap<>();

    public WebSite getWebSiteCategory(String type){
        if(!pool.containsKey(type)){
            pool.put(type, new ConcreteWebSite(type));
        }
        return pool.get(type);
    }

    //获取网站分类的总数 (池中有多少个网站类型)
    public int getWebSiteCount() {
        return pool.size();
    }
}
