package com.atguigu.memento.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Memento {
    //攻击力
    private int vit;
    //防御力
    private int def;


}
