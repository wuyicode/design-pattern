package com.atguigu.singleton;

public class SingletonTest03 {
    public static void main(String[] args) {
        Singleton03 instance1 = Singleton03.getInstance();
        Singleton03 instance2 = Singleton03.getInstance();

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() +"\t"+ instance2.hashCode());
    }

}


class Singleton03 {
    // 构造函数私有化
    private Singleton03(){}
    private static Singleton03 instance;

    // 注意，这里线程不安全。
    public static Singleton03 getInstance(){
        if(instance==null)
            instance = new Singleton03();
        return instance;
    }

}