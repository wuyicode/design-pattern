package com.atguigu.singleton;

public class SingletonTest07 {
    public static void main(String[] args) {
        Singleton07 instance1 = Singleton07.getInstance();
        Singleton07 instance2 = Singleton07.getInstance();

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() + "\t" + instance2.hashCode());
    }

}


class Singleton07 {
    // 构造函数私有化
    private Singleton07() {
    }
    // volatile保证线程的可见性， 效果更好。
    private static volatile Singleton07 instance;

    /**
     * 静态内部类 实现了懒加载和线程安全。
     */
    private static class SingletonInstance {
        private static final Singleton07 INSTANCE = new Singleton07();
    }

    public static synchronized Singleton07 getInstance(){
        return SingletonInstance.INSTANCE;
    }

}