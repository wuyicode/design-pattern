package com.atguigu.singleton;

public class SingletonTest01 {
    public static void main(String[] args) {
        Singleton01 instance1 = Singleton01.getInstance();
        Singleton01 instance2 = Singleton01.getInstance();

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() +"\t"+ instance2.hashCode());
    }

}

/**
 *  静态常量方法
 *  缺点是没有懒加载，有可能造成浪费。
 *  优点是，线程安全。
 */
class Singleton01 {
    // 构造函数私有化
    private Singleton01(){}
    private static final Singleton01 instance = new Singleton01();
    public static Singleton01 getInstance(){
        return instance;
    }

}